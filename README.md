# requirements
/!\ You need to have the [Kalliope Core](https://github.com/kalliope-project/kalliope) installed before cloning this starter kit.

cd kalliope 
python3 install setup.py

# kalliope starter config en

This is an out of the box working configuration for english akira user

How to use
 ```bash
git clone git clone https://gitlab.com/divyabhanu72/akira_demo.git
cd akira_demo
kalliope start
```
